package be.kdg.state;

public class StateHuman extends StateEntity implements CreatureState {
    public StateHuman(Creature creature) {
        super(creature);
    }

    @Override
    public String greet() {
        return "Darling!";
    }

    @Override
    public void kiss() {
        System.out.println("Op de wang of op de mond?");
    }
}
