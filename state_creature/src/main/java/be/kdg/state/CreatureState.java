package be.kdg.state;

public interface CreatureState {
    String greet();
    void kiss();
}
