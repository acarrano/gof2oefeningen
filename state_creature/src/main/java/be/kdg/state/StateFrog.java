package be.kdg.state;

public class StateFrog extends StateEntity implements CreatureState {
    public StateFrog(Creature creature) {
        super(creature);
    }

    @Override
    public String greet() {
        return "Ribbet!";
    }

    @Override
    public void kiss() {
        getCreature().setFrog(false);
        getCreature().setCreatureState(new StateHuman(getCreature()));
    }
}
