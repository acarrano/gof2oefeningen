package be.kdg.state;

/**
 * Het gedrag van de methode greet is afhankelijk van de toestande van de boolean isFrog.
 * Pas op deze klasse het State pattern toe (maak gebruik van een interface).
 */
public class Creature implements CreatureState {
    private boolean isFrog = true;
    CreatureState state;

    public Creature() {
        setCreatureState(new StateFrog(this));
    }

    public String greet() {
        return state.greet();
    }

    public void kiss() {
        state.kiss();
    }

    public void setCreatureState(CreatureState state) { this.state = state; }

    public boolean isFrog() {
        return isFrog;
    }

    public void setFrog(boolean frog) {
        isFrog = frog;
    }
}
