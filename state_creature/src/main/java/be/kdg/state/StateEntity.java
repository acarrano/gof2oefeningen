package be.kdg.state;

public abstract class StateEntity {
    private Creature creature;

    public StateEntity(Creature creature) {
        this.creature = creature;
    }

    public Creature getCreature() {
        return creature;
    }
}
